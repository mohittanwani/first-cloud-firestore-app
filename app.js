
//Configuration		
var config = {
		apiKey: "AIzaSyD8jctcs4_2fG-cU0g3fV72OfRqr6stcT8",
		authDomain: "firestore-to-do.firebaseapp.com",
		databaseURL: "https://firestore-to-do.firebaseio.com",
		projectId: "firestore-to-do",
};
//Firebase app initialization
firebase.initializeApp(config);

var todoText = document.querySelector("#todoText");
var addButton = document.querySelector("#addButton");
var listTodos = document.querySelector("#listTodos");

todoText.addEventListener('keyup', handleSubmit);
addButton.addEventListener('click', handleSubmit);

//Database ref
const todoRef = firebase.firestore().collection("todos");

//Handle submit
function handleSubmit(e) {
	
  if (e.keyCode !== 13 && e.type != "click") {
    return;
  }
  const todo = todoText.value;
  addButton.innerHTML = "Adding...";

  if (todo === "") {
    return;
  }

  //Add to the database
  todoRef.add({
    title: todo,
    checked: false,
    createdAt: (new Date()).getTime()
  }).then(function(docRef) {
    todoText.value = "";
    addButton.innerHTML = "Add";
  }).catch(function(error) {
    console.log(error);
  })
}
// Listener for rendering todos
todoRef.orderBy("createdAt", 'desc').onSnapshot(function(docSnapShot) {
  listTodos.innerHTML = "";
  docSnapShot.forEach(function(doc) {
    todo = doc.data();
    todo.id = doc.id;

    //Container to append the child for HTML
    var container = document.createElement("p");
  
    //Checkbox 
    var checkBox = document.createElement("input");
    checkBox.setAttribute("type", "checkbox");
    checkBox.setAttribute("data-id", todo.id);
    checkBox.checked = todo.checked;
    checkBox.addEventListener('change', handleCheckToggle);
    
    //Title Block
    var titleBlock = document.createElement("span");
    titleBlock.innerHTML =   "&nbsp;" + todo.title + "&nbsp;";

    //Append the childs
    container.appendChild(checkBox);
    container.appendChild(titleBlock);
    
    //Append to the list on HTML
    listTodos.appendChild(container);
  })
})
//On checkbox click update database
function handleCheckToggle(e) {
  var targetElement = e.target;
  var checked = targetElement.checked;
  var id = targetElement.dataset.id;
  todoRef.doc(id).update({
    checked: checked,
  })
}